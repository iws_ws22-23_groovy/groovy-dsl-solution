import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.customizers.ImportCustomizer
import org.codehaus.groovy.control.customizers.SecureASTCustomizer

abstract class ScriptBase extends Script {
    String name
    abstract void scriptBody()
    def run() {
        println "#### Started evaluating script ####"
        scriptBody()
        println "#### Done evaluating script ####"
    }

    def answer() { println "General $name?!"}
    def greeter() { println "Hello there!" }
}

// Customize imports
def icz = new ImportCustomizer()
icz.addImports('java.time.LocalTime')

// Customize AST
def scz = new SecureASTCustomizer()
scz.with {allowedImports = ['java.time.LocalTime']}

// set config
def config = new CompilerConfiguration()
config.scriptBaseClass = 'ScriptBase'
config.addCompilationCustomizers(icz, scz)

def shell = new GroovyShell(this.class.classLoader, config)

// execute script
shell.evaluate """
    setName 'Kenobi'
    greeter()
    answer()
    println "The current time is: " + LocalTime.now() // TODO: Import LocalTime into Script
"""