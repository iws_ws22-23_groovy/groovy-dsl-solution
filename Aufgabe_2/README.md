# Aufgabe 2

In der Groovy Datei `RunScript.groovy` findest du ein einfaches Skript und eine Script Base Klasse welche die Laufzeitumgebung des Skripts beeinflusst.
Passe den Compiler für das Skript nun so an das, das Java-Package [java.time.LocalTime](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/time/LocalTime.html) implizit in dem Skript importiert wird.
Außerdem sollst du den Import von anderen Packages untersagen.

**Aufgabe: Sorge das LocalTime immer in das Skript importiert wird und alle anderen Imports unterbunden werden**

_Solltest du früher fertig sein könntest du das Skript einschränken so das keine Funktionen oder Closures mehr in dem Skript definiert werden können_