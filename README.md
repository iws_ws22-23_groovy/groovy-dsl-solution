# Domain Specific Languages (DSL)

Groovy bietet einige Sprachfeatures, welche es einfach machen eine eigene [Domain Specific Language](https://en.wikipedia.org/wiki/Domain-specific_language) zu erstellen.
Details dazu kann man in der [offiziellen Dokumentation](http://docs.groovy-lang.org/docs/latest/html/documentation/core-domain-specific-languages.html) zu Groovy nachlesen.

In den Unterordnern findest du die Übungen zu diesem Abschnitt.