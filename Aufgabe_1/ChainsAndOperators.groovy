// #### Command chains ####
show = {println it}
randomNumber = {new Random().nextInt()}

def please(action) {
    [a: { what ->
        [greaterThan: { n ->
            int num = what()
            if(num > n)
                action(num)
            else
                action("No greater Number found!")
        },
         lessThan: { n ->
             int num = what()
             if(num < n)
                 action(num)
             else
                 action("No smaller Number found!")
         }]
    }]
}

// make this executable
please show a randomNumber greaterThan 5
please show a randomNumber lessThan 5


// #### Operator Overloading ####

class MyString {
    private String internalString

    MyString(String s) { internalString = s }

    @Override
    String toString() { return internalString }

    MyString next() {
        internalString += " "
        return this
    }
}

MyString a = new MyString("Hello World!")

println "'$a'" // expecting 'Hello World!'
a++ // increment String
print "'$a'" // expecting 'Hello World! '