# Aufgabe 1

In der Groovy Datei `ChainsAndOperators.groovy` findest du zwei Aufgaben, welche du lösen sollst.
Bei der ersten Aufgabe zum Thema *Command Chains* sollst du zwei Sätze welche stark an natürliche Sprache erinnern durch die Verwendung von *Command Chains* in ausführbare Anweisungen verwandeln.
Bei der zweiten Aufgabe zum Thema *Operator overloading* sollst du einen Increment Operator für unsere eigene String Klasse bauen.
Die Klasse ist dir bereits vorgegeben.
Wenn ein String inkrementiert wird, dann wird einfach an sein Ende ein Leerzeichen angehängt.

**Aufgabe: Mache die "DSL-Anweisungen" ausführbar**

_Solltest du früher fertig sein kannst du gerne weitere Operatoren für unsere Strings implementieren_